describe("Travel Insurance", function() {
  it("Find user quote", function() {
    cy.server();
    cy.route("POST", "https://travelinsurance.api.finder.com.au/1/quote").as(
      "quoteRequest"
    );

    let destination = "New Zealand";
    let age = "30";
    let email = "passenger@example.com";

    // Given the user is on Travel Insurance page
    cy.visit("/");

    // When the user types the destination
    cy.get(".fti-destination input:visible")
      .click()
      .type(destination)
      .type("{enter}");

    // and the user opens start date picker
    cy.get('.fti-single input[name="fti_from"]')
      .as("startDateInput")
      .click();

    // and the user selects first available start date (today)
    cy.get(".day:not(.old):not(.disabled)")
      .eq(0)
      .click();

    // and the user opens end date picker
    cy.get('.fti-to-wrap input[name="fti_to"]')
      .as("endDateInput")
      .click();

    // and the user goes to next month
    cy.get(".datepicker-days .next").click();

    // and the user selects last available end date
    cy.get(".day:not(.new):not(.disabled)")
      .last()
      .click();

    // and the user enters their age
    cy.get(".fti-age[name=traveller_age0]:visible")
      .click()
      .type(age);

    // and the user types the email adress (optional)
    cy.get(".form-group input[name='fti_email']:visible")
      .click()
      .type(email);

    // and the user signs up for the travel newsletter (optional)
    cy.get(".fti-single #fti_newsletter")
      .check()
      .should("be.checked");

    // and the user clicks on submit button
    cy.get(".fin-btn")
      .contains("Get my quote")
      .click();

    cy.wait("@quoteRequest");

    // Then the user should see their quote
    cy.get(".fti-result-table tbody tr")
      .its("length")
      .should("be.gt", 0);

    // and the destination information should be presented
    cy.get(".weight-semi-bold:eq(1)").should("contain", destination);
  });
});
